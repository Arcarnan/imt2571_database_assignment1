<?php
include_once("IModel.php");
include_once("Book.php");
//include_once("view/ErrorView.php");

/*
* @author Rune Hjelsvold + Nataniel Gåsøy
* Rewritten with much help from Sindre Blomberg Garvik for the second attemt
* @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
*/
class DBModel implements IModel
{
  protected $db = null;

  public function __construct($db = null)
  {
    if ($db)
    {
      $this->db = $db;
    }
    else
    {
      $this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8mb4');
    }
  }

  /** Function returning the complete list of books in the collection. Books are
  * returned in order of id.
  * @return Book[] An array of book objects indexed and ordered by their id.
  * @throws PDOException
  */
  public function getBookList()
  {
    $booklist = array();
    $table_name = 'book';
    $test = "SELECT 1 FROM " . $table_name . " LIMIT 1";
    $test = $this->db->query($test);

    if($test)
    {
      foreach($qry = $this->db->query("SELECT * FROM `book`") as $row)
      {
        $booklist [] = new Book ($row['title'], $row['author'], $row['description'], $row['id']);
      }
    }
    else
    {
      $error = 'could not find table';
      $view = new ErrorView();
      $view->create();
      //throw new Exception ($error);
    }
    return $booklist;
  }

  /** Function retrieving information about a given book in the collection.
  * @param integer $id the id of the book to be retrieved
  * @return Book|null The book matching the $id exists in the collection; null otherwise.
  * @throws PDOException
  */
  public function getBookById($id)
  {
    $book = null;
    $table_name = 'book';
    $test = "SELECT 1 FROM " . $table_name . " LIMIT 1";
    $test = $this->db->query($test);
    if($test)
    {
      if (is_numeric($id))
      {
        if ($id>=0)
        {
          $qry = $this->db->prepare("SELECT * FROM book WHERE id=?");
          $qry->execute(array($id));
          $row = $qry->fetch(PDO::FETCH_ASSOC);
          $book = new book($row['title'], $row['author'], $row['description'], $row['id']);

          if($book->title == "" || $book->author == "")
          {
            return NULL;
          }
        }
        else
        {
          return NULL;
        }
      }
    else
    {
      $error = 'invalid ID';
      $view = new ErrorView();
      $view->create();
      //throw new Exception ($error);
    }
  }
  else
  {
    $error = 'could not find table';
    $view = new ErrorView();
    $view->create();
    //throw new Exception ($error);
  }
  return $book;
}

/** Adds a new book to the collection.
* @param $book Book The book to be added - the id of the book will be set after successful insertion.
* @throws PDOException
*/
public function addBook($book)
{
  if ((!empty($book->title)) && (!empty($book->author)))
  {
    $qry = $this->db->prepare
    (
      "INSERT INTO book (title, author, description)
      VALUES (?, ?, ?)"
    );
    $qry->execute(array(
      $book->title,
      $book->author,
      $book->description)
    );
    $book->id = $this->db->lastInsertId();
    return true;
  }
  else
  {
    if (($book->title == null) && ($book->author == null))
    {
      echo " *** Title and author cannot be empty. ***";
    }
    else if ($book->title == null)
    {
      echo " *** Title cannot be empty. ***";
    }
    else
    {
      echo " *** Author cannot be empty. ***";
    }
    return false;
  }
}

/** Modifies data related to a book in the collection.
* @param $book Book The book data to be kept.
* @todo Implement function using PDO and a real database.
*/
public function modifyBook($book)
{
  if (($book->title !="") && ($book->author !="") && ($book->id >= 0) && (is_numeric($book->id)))
  {
    $qry = $this->db->prepare
    ("UPDATE book
      SET title = ?,
      author = ?,
      description = ?
      WHERE id = ?");

      $qry->execute(array(
        $book->title,
        $book->author,
        $book->description,
        $book->id)
      );
    }
    else
      {
        if (($book->title == null) && ($book->author == null))
        {
          echo " *** Title and author cannot be empty. *** ";
          throw new Exception ($error);
        }
        else if ($book->title == null)
        {
          echo " *** Title cannot be empty. ***";
          throw new Exception ($error);
        }
        else if ($book->author == null)
        {
          echo " *** author cannot be empty. *** ";
          throw new Exception ($error);
        }
        else if ($id<=0)
        {
          echo " *** ID must be positive. ***";
          throw new Exception ($error);
        }
        else if (!is_numeric($id))
        {
          echo " *** ID must be numeric. ***";
          throw new Exception ($error);
        }
        else
        {
          echo " *** Author cannot be empty. ***";
          throw new Exception ($error);
        }
      }
      return true;
  }

  /** Deletes data related to a book from the collection.
  * @param $id integer The id of the book that should be removed from the collection.
  */
  public function deleteBook($id)
  {
    if (($id>=0) && (is_numeric($id)))
    {
      $qry = $this->db->prepare
      (
        "DELETE FROM book
        WHERE id = ?"
      );
      $qry->execute(array($id));
    }
    else
    {
      if ($id<=0)
      {
        echo " *** ID must be positive. ***";
      }
      else if (!is_numeric($id))
      {
        echo " *** ID must be numeric. ***";
      }
    }
  }
}
?>
