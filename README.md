### What is this repository for? ###

Assignment 1 in IMT2571 - databases.

### Task text ###
-----------------UPDATE:​ ​ Hand​ ​ in​ ​ - ​ ​ second​ ​ attempt----------------
I​ ​ was​ ​ not​ ​ quite​ ​ sure​ ​ what​ ​ was​ ​ and​ ​ was​ ​ not​ ​ accepted​ ​ from​ ​ my​ ​ first​ ​ hand​ ​ in,​ ​ other​ ​ than​ ​ that​ ​ it
had​ ​ to​ ​ do​ ​ with​ ​ my​ ​ code​ ​ and​ ​ with​ ​ validation.​ ​ The​ ​ feedback​ ​ said:
“ ​ To ​ ​ pass ​ ​ second ​ ​ attempt ​ ​ you ​ ​ need ​ ​ to:
Make ​ ​ sure ​ ​ your ​ ​ code ​ ​ is ​ ​ working ​ ​ properly. ​ ​ Your ​ ​ code ​ ​ has ​ ​ all ​ ​ the ​ ​ validations ​ ​ required.”
Thus,​ ​ i ​ ​ have​ ​ rewritten​ ​ most​ ​ of​ ​ the​ ​ code​ ​ i ​ ​ previously​ ​ handed​ ​ in,​ ​ with​ ​ much​ ​ help​ ​ from​ ​ my
classmate​ ​ Sindre​ ​ Blomberg​ ​ Garvik.​ ​ One​ ​ of​ ​ the​ ​ biggest​ ​ changes​ ​ is​ ​ the​ ​ fact​ ​ that​ ​ i ​ ​ have
removed​ ​ all​ ​ of​ ​ the​ ​ try​ ​ catch​ ​ clauses,​ ​ and​ ​ replaced​ ​ them​ ​ with​ ​ if-else​ ​ statements.​ ​ i ​ ​ have​ ​ also
rewritten​ ​ most​ ​ of​ ​ the​ ​ things​ ​ in​ ​ the​ ​ unit​ ​ tests​ ​ file,​ ​ as​ ​ it​ ​ seems​ ​ i ​ ​ had​ ​ misunderstood​ ​ that​ ​ part​ ​ of
the​ ​ task.​ ​ i ​ ​ have​ ​ included​ ​ the​ ​ screenshot​ ​ of​ ​ my​ ​ unit​ ​ tests​ ​ to​ ​ show​ ​ that​ ​ they​ ​ all​ ​ passed​ ​ (the
tests​ ​ triggered​ ​ some​ ​ error​ ​ messages​ ​ from​ ​ my​ ​ DBModel,​ ​ which​ ​ is​ ​ to​ ​ be​ ​ expected​ ​ as​ ​ the​ ​ unit
tests​ ​ also​ ​ tests​ ​ for​ ​ illegal​ ​ activity).
The​ ​ functional​ ​ tests​ ​ are​ ​ all​ ​ also​ ​ working​ ​ properly​ ​ now,​ ​ where​ ​ all​ ​ tests​ ​ pass​ ​ without
complaints​ ​ (see​ ​ screenshot​ ​ below).
-------------------Hand​ ​ in​ ​ - ​ ​ first​ ​ attempt(unchanged)---------------
Part​ ​ I ​ ​ – ​ ​ Getting​ ​ Familiar​ ​ with​ ​ MySQL/MariaDB​ ​ and
phpMyAdminThis​ ​ part​ ​ was​ ​ pretty​ ​ straight-forward.​ ​ After​ ​ downloading,​ ​ uninstalling​ ​ because​ ​ things​ ​ went
wrong​ ​ and​ ​ reinstalling​ ​ (and​ ​ getting​ ​ some​ ​ help​ ​ from​ ​ classmates)​ ​ i ​ ​ managed​ ​ to​ ​ download
xampp,​ ​ starting​ ​ it​ ​ up​ ​ and​ ​ create​ ​ the​ ​ database​ ​ in​ ​ myPhpAdmin.
when​ ​ setting​ ​ up​ ​ the​ ​ database,​ ​ i ​ ​ misinterpreted​ ​ the​ ​ task​ ​ and​ ​ ended​ ​ up​ ​ writing​ ​ “500​ ​ utf8”​ ​ in
the​ ​ length​ ​ setting,​ ​ instead​ ​ of​ ​ doing​ ​ it​ ​ the​ ​ right​ ​ way.​ ​ this​ ​ was​ ​ randomly​ ​ corrected​ ​ by​ ​ a ​ ​ fellow
student​ ​ the​ ​ week​ ​ after.​ ​ apart​ ​ from​ ​ that,​ ​ and​ ​ the​ ​ initial​ ​ start​ ​ of​ ​ figuring​ ​ out​ ​ how​ ​ to​ ​ actually​ ​ set
up​ ​ the​ ​ thing,​ ​ the​ ​ rest​ ​ of​ ​ this​ ​ step​ ​ (including​ ​ the​ ​ SQL​ ​ queries).
Part​ ​ II​ ​ – ​ ​ Getting​ ​ Familiar​ ​ with​ ​ HTML,​ ​ PHP​ ​ and​ ​ PDO
not​ ​ really​ ​ much​ ​ to​ ​ say​ ​ about​ ​ this​ ​ step,​ ​ other​ ​ than​ ​ the​ ​ fact​ ​ that​ ​ it​ ​ took​ ​ a ​ ​ very​ ​ long​ ​ time​ ​ to​ ​ work
through​ ​ all​ ​ of​ ​ the​ ​ tutorials.
Part​ ​ III​ ​ – ​ ​ Creating​ ​ a ​ ​ Database​ ​ Application
When​ ​ downloading​ ​ the​ ​ current​ ​ app​ ​ from​ ​ and​ ​ testing​ ​ it,​ ​ i ​ ​ got​ ​ slightly​ ​ confused​ ​ as​ ​ to​ ​ where​ ​ i
was​ ​ meant​ ​ to​ ​ download​ ​ it​ ​ to,​ ​ and​ ​ therefore​ ​ had​ ​ to​ ​ change​ ​ it​ ​ later​ ​ to​ ​ the​ ​ htdocs​ ​ file.​ ​ after
doing​ ​ this,​ ​ and​ ​ searching​ ​ a ​ ​ good​ ​ while​ ​ for​ ​ the​ ​ phpunit​ ​ file​ ​ to​ ​ rename​ ​ it,​ ​ i ​ ​ managed​ ​ to​ ​ run​ ​ the
functional​ ​ test​ ​ successfully.
Creating​ ​ the​ ​ code,​ ​ however,​ ​ that​ ​ is​ ​ where​ ​ the​ ​ real​ ​ trouble​ ​ began.​ ​ this​ ​ took​ ​ many​ ​ days​ ​ of
work​ ​ to​ ​ do,​ ​ i ​ ​ was​ ​ not​ ​ quite​ ​ sure​ ​ where​ ​ to​ ​ even​ ​ start​ ​ and​ ​ the​ ​ tutorials​ ​ were​ ​ only​ ​ semi-helpful,
as​ ​ most​ ​ of​ ​ them​ ​ had​ ​ a ​ ​ very​ ​ different​ ​ setup​ ​ from​ ​ what​ ​ we​ ​ were​ ​ supposed​ ​ to​ ​ use​ ​ in​ ​ this​ ​ task.
This​ ​ took​ ​ a ​ ​ lot​ ​ of​ ​ searching,​ ​ a ​ ​ lot​ ​ of​ ​ googling,​ ​ a ​ ​ lot​ ​ of​ ​ trial​ ​ and​ ​ error​ ​ and​ ​ a ​ ​ lot​ ​ of​ ​ help​ ​ from
fellow​ ​ students.​ ​ i ​ ​ do,​ ​ however,​ ​ believe​ ​ that​ ​ my​ ​ code​ ​ is​ ​ adequate.​ ​ i ​ ​ have​ ​ testet​ ​ it​ ​ many​ ​ times,
and​ ​ everything​ ​ seems​ ​ to​ ​ be​ ​ working​ ​ as​ ​ it​ ​ should.​ ​ i ​ ​ was​ ​ uncertain​ ​ about​ ​ the​ ​ “error​ ​ page​ ​ being
returned”​ ​ part,​ ​ and​ ​ after​ ​ a ​ ​ lot​ ​ of​ ​ searching​ ​ on​ ​ the​ ​ web​ ​ am​ ​ assuming​ ​ that​ ​ you​ ​ mean​ ​ the
appropriate​ ​ error​ ​ code​ ​ is​ ​ being​ ​ displayed​ ​ when​ ​ there​ ​ is​ ​ an​ ​ invalid​ ​ input.
gonna​ ​ be​ ​ completely​ ​ honest​ ​ here,​ ​ not​ ​ sure​ ​ whether​ ​ you​ ​ want​ ​ the​ ​ web​ ​ link​ ​ or​ ​ the​ ​ clone​ ​ repo
link,​ ​ so​ ​ here​ ​ is​ ​ both:
weblink:​ ​ https://bitbucket.org/Arcarnan/imt2571_assignment_1
Git​ ​ repo:​ ​ https://Arcarnan@bitbucket.org/Arcarnan/imt2571_assignment_1.git

